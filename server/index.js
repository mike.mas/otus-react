const express = require('express');
const cors = require('cors')
const app = express();
const port = 5000;
app.use(cors());
app.use(express.json());

app.post('/login', (req, res) => {
   
    if (req.body.login == "test" && req.body.password == "test")
        res.send({authToken: 'test token'});
    else 
        res.status(401).send("Wrong credentials");
    
});

app.listen(port, () => console.log(`authApi app listening on port ${port}!`))
