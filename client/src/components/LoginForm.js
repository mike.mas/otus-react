import React, { useState } from "react";
import {authService} from '../services/authService';

function LoginForm() {
    
  const [login, setLogin] = useState("");
  const [password, setPassword] = useState("");
  const [message, setMessage] = useState("");

  const handleSubmit = async (e) => {
    e.preventDefault();
    try {
        const res  = await authService.login({login, password});
        setMessage("Your token: " + res.data.authToken);
    } catch (err)
    {   
       setMessage( err.message);
    }
}
  
  return (
      <form onSubmit={handleSubmit} className="login-form">
      <div>{message}</div>

      <div>
        <input type="text" name="login" value={login} onChange={e => setLogin(e.target.value)} placeholder="Login" />
      </div>

      <div>
        <input type="password" name="password" value={password} onChange={e => setPassword(e.target.value)}  placeholder="Password" />
      </div>

      <div>
        <button type="submit">Login</button>

      </div>

      </form>
    );
  }
  
  export default LoginForm;
  